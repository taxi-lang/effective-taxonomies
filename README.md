# effective-taxonomies

A repository of practices and principals for building effective Taxonomy strategies with Taxi and Vyne
This repository is inspired by [Principled GraphQl](https://principledgraphql.com/). We agree with many of the principles outlined 
on that site, but wish to explore Taxonomy prinicples

## Layout

This repository is a series of principals and practices, each written as it's
own standalone markdown file.

It's packaged and published as a searchable site using gatsby